<h2 id="http-post">HTTP POST</h2>
<p>POST is the HTTP method that was invented to send data to a receiving web application, and it is how most common HTML forms on the web works. It usually sends a chunk of relatively small amounts of data to the receiver.</p>
<p>When the data is sent by a browser after data have been filled in a form, it will send it “URL encoded”, as a serialized name=value pairs separated with ampersand symbols (‘&amp;’). You send such data with curl’s <code>-d</code> or <code>--data</code> option like this:</p>
<pre><code>curl -d &#39;name=admin&amp;shoesize=12&#39; http://example.com/</code></pre>
<p>When specifying multiple <code>-d</code> options on the command line, curl will concatenate them and insert ampersands in between, so the above example could also be made like this:</p>
<pre><code>curl -d name=admin -d shoesize=12 http://example.com/</code></pre>
<p>If the amount of data to send is not really fit to put in a mere string on the command line, you can also read it off a file name in standard curl style:</p>
<pre><code>curl -d @filename http://example.com</code></pre>
<h3 id="content-type">Content-Type</h3>
<p>POSTing with curl’s -d option will make it include a default header that looks like <code>Content-Type: application/x-www-form-urlencoded</code>. That’s what your typical browser will use for a plain POST.</p>
<p>Many receivers of POST data do not care about or check the Content-Type header.</p>
<p>If that header is not good enough for you, you should, of course, replace that and instead provide the correct one. Such as if you POST JSON to a server and want to more accurately tell the server about what the content is:</p>
<pre><code>curl -d &#39;{json}&#39; -H &#39;Content-Type: application/json&#39; https://example.com</code></pre>
<h3 id="posting-binary">POSTing binary</h3>
<p>When reading from a file, <code>-d</code> will strip out carriage return and newlines. Use <code>--data-binary</code> if you want curl to read and use the given file in binary exactly as given:</p>
<pre><code>curl --data-binary @filename http://example.com/</code></pre>
<h3 id="url-encoding">URL encoding</h3>
<p>Percent-encoding, also known as URL encoding, is technically a mechanism for encoding data so that it can appear in URLs. This encoding is typically used when sending POSTs with the <code>application/x-www-form-urlencoded</code> content type, such as the ones curl sends with <code>--data</code> and <code>--data-binary</code> etc.</p>
<p>The command-line options mentioned above all require that you provide properly encoded data, data you need to make sure already exists in the right format. While that gives you a lot of freedom, it is also a bit inconvenient at times.</p>
<p>To help you send data you have not already encoded, curl offers the <code>--data-urlencode</code> option. This option offers several different ways to URL encode the data you give it.</p>
<p>You use it like <code>--data-urlencode data</code> in the same style as the other –data options. To be CGI-compliant, the <strong>data</strong> part should begin with a name followed by a separator and a content specification. The <strong>data</strong> part can be passed to curl using one of the following syntaxes:</p>
<ul class="incremental">
<li><p>“content”: This will make curl URL encode the content and pass that on. Just be careful so that the content does not contain any = or @ symbols, as that will then make the syntax match one of the other cases below!</p></li>
<li><p>“=content”: This will make curl URL encode the content and pass that on. The initial ‘=’ symbol is not included in the data.</p></li>
<li><p>“name=content”: This will make curl URL encode the content part and pass that on. Note that the name part is expected to be URL encoded already.</p></li>
<li><p>“<span class="citation" data-cites="filename">@filename</span>”: This will make curl load data from the given file (including any newlines), URL encode that data and pass it on in the POST.</p></li>
<li><p>“name@filename”: This will make curl load data from the given file (including any newlines), URL encode that data and pass it on in the POST. The name part gets an equal sign appended, resulting in name=urlencoded-file-content. Note that the name is expected to be URL encoded already.</p></li>
</ul>
<p>As an example, you could POST a name to have it encoded by curl:</p>
<pre><code>curl --data-urlencode &quot;name=John Doe (Junior)&quot; http://example.com</code></pre>
<p>…which would send the following data in the actual request body:</p>
<pre><code>name=John%20Doe%20%28Junior%29</code></pre>
<p>If you store the string <code>John Doe (Junior)</code> in a file named <code>contents.txt</code>, you can tell curl to send that contents URL encoded using the field name ‘user’ like this:</p>
<pre><code>curl --data-urlencode user@contents.txt http://example.com</code></pre>
<p>In both these examples above, the field name is not URL encoded but is passed on as-is. If you want to URL encode the field name as well, like if you want to pass on a field name called “user name”, you can ask curl to encode the entire string by prefixing it with an equals sign (that will not get sent):</p>
<pre><code>curl --data-urlencode &quot;=user name=John Doe (Junior)&quot; http://example.com</code></pre>
<h3 id="convert-that-to-a-get">Convert that to a GET</h3>
<p>A little convenience feature that could be suitable to mention in this context (even though it is not for POSTing), is the <code>-G</code> or <code>--get</code> option, which takes all data you have specified with the different <code>-d</code> variants and appends that data on the right end of the URL separated with a ‘?’ and then makes curl send a GET instead.</p>
<p>This option makes it easy to switch between POSTing and GETing a form, for example.</p>
<h3 id="expect-100-continue">Expect 100-continue</h3>
<p>HTTP has no proper way to stop an ongoing transfer (in any direction) and still maintain the connection. So, if we figure out that the transfer had better stop after the transfer has started, there are only two ways to proceed: cut the connection and pay the price of reestablishing the connection again for the next request, or keep the transfer going and waste bandwidth but be able to reuse the connection next time.</p>
<p>One example of when this can happen is when you send a large file over HTTP, only to discover that the server requires authentication and immediately sends back a 401 response code.</p>
<p>The mitigation that exists to make this scenario less frequent is to have curl pass on an extra header, <code>Expect: 100-continue</code>, which gives the server a chance to deny the request before a lot of data is sent off. curl sends this Expect: header by default if the POST it will do is known or suspected to be larger than just minuscule. curl also does this for PUT requests.</p>
<p>When a server gets a request with an 100-continue and deems the request fine, it will respond with a 100 response that makes the client continue. If the server does not like the request, it sends back response code for the error it thinks it is.</p>
<p>Unfortunately, lots of servers in the world do not properly support the Expect: header or do not handle it correctly, so curl will only wait 1000 milliseconds for that first response before it will continue anyway.</p>
<p>Those are 1000 wasted milliseconds. You can then remove the use of Expect: from the request and avoid the waiting with <code>-H</code>:</p>
<pre><code>curl -H Expect: -d &quot;payload to send&quot; http://example.com</code></pre>
<p>In some situations, curl will inhibit the use of the Expect header if the content it is about to send is small (like below one kilobyte), as having to “waste” such a small chunk of data is not considered much of a problem.</p>
<h3 id="chunked-encoded-posts">Chunked encoded POSTs</h3>
<p>When talking to a HTTP 1.1 server, you can tell curl to send the request body without a <code>Content-Length:</code> header upfront that specifies exactly how big the POST is. By insisting on curl using chunked Transfer-Encoding, curl will send the POST “chunked” piece by piece in a special style that also sends the size for each such chunk as it goes along.</p>
<p>You send a chunked POST with curl like this:</p>
<pre><code>curl -H &quot;Transfer-Encoding: chunked&quot; -d &quot;payload to send&quot; http://example.com</code></pre>
<h3 id="hidden-form-fields">Hidden form fields</h3>
<p>This chapter has explained how sending a post with <code>-d</code> is the equivalent of what a browser does when an HTML form is filled in and submitted.</p>
<p>Submitting such forms is a common operation with curl; effectively, to have curl fill in a web form in an automated fashion.</p>
<p>If you want to submit a form with curl and make it look as if it has been done with a browser, it is important to provide all the input fields from the form. A common method for web pages is to set a few hidden input fields to the form and have them assigned values directly in the HTML. A successful form submission, of course, also includes those fields and in order to do that automatically you may be forced to first download the HTML page that holds the form, parse it, and extract the hidden field values so that you can send them off with curl.</p>
<h3 id="figure-out-what-a-browser-sends">Figure out what a browser sends</h3>
<p>A common shortcut is to simply fill in the form with your browser and submit it and check in the browser’s network development tools exactly what it sent.</p>
<p>A slightly different way is to save the HTML page containing the form, and then edit that HTML page to redirect the ‘action=’ part of the form to your own server or a test server that just outputs exactly what it gets. Completing that form submission will then show you exactly how a browser sends it.</p>
<p>A third option is, of course, to use a network capture tool such as Wireshark to check exactly what is sent over the wire. If you are working with HTTPS, you cannot see form submissions in clear text on the wire but instead you need to make sure you can have Wireshark extract your TLS private key from your browser. See the Wireshark documentation for details on doing that.</p>
<h3 id="javascript-and-forms">JavaScript and forms</h3>
<p>A common mitigation against automated “agents” or scripts using curl is to have the page with the HTML form use JavaScript to set values of some input fields, usually one of the hidden ones. Often, there’s some JavaScript code that executes on page load or when the submit button is pressed which sets a magic value that the server then can verify before it considers the submission to be valid.</p>
<p>You can usually work around that by just reading the JavaScript code and redoing that logic in your script. Using the above mentioned tricks to check exactly what a browser sends is then also a good help.</p>
