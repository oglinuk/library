<h1 id="http-requests">HTTP Requests</h1>
<p>A HTTP request is what curl sends to the server when it tells the server what to do. When it wants to get data or send data. All transfers involving HTTP starts with a HTTP request.</p>
<p>A HTTP request contains a method, a path, HTTP version and a set of request headers. And of course a libcurl using application can tweak all those fields.</p>
<h2 id="request-method">Request method</h2>
<p>Every HTTP request contains a “method”, sometimes referred to as a “verb”. It is usually something like GET, HEAD, POST or PUT but there are also more esoteric ones like DELETE, PATCH and OPTIONS.</p>
<p>Usually when you use libcurl to set up and perform a transfer the specific request method is implied by the options you use. If you just ask for a URL, it means the method will be <code>GET</code> while if you set for example <code>CURLOPT_POSTFIELDS</code> that will make libcurl use the <code>POST</code> method. If you set <code>CURLOPT_UPLOAD</code> to true, libcurl will send a <code>PUT</code> method in its HTTP request and so on. Asking for <code>CURLOPT_NOBODY</code> will make libcurl use <code>HEAD</code>.</p>
<p>However, sometimes those default HTTP methods are not good enough or simply not the ones you want your transfer to use. Then you can instruct libcurl to use the specific method you like with <code>CURLOPT_CUSTOMREQUEST</code>. For example, you want to send a <code>DELETE</code> method to the URL of your choice:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, &quot;DELETE&quot;);
curl_easy_setopt(curl, CURLOPT_URL, &quot;https://example.com/file.txt&quot;);</code></pre>
<p>The CURLOPT_CUSTOMREQUEST setting should only be the single keyword to use as method in the HTTP request line. If you want to change or add additional HTTP request headers, see the following section.</p>
<h2 id="customize-http-request-headers">Customize HTTP request headers</h2>
<p>When libcurl issues HTTP requests as part of performing the data transfers you’ve asked it to, it will of course send them off with a set of HTTP headers that are suitable for fulfilling the task given to it.</p>
<p>If just given the URL “http://localhost/file1.txt”, libcurl 7.51.0 would send the following request to the server:</p>
<pre><code>GET /file1.txt HTTP/1.1
Host: localhost
Accept: */*</code></pre>
<p>If you would instead instruct your application to also set <code>CURLOPT_POSTFIELDS</code> to the string “foobar” (6 letters, the quotes only used for visual delimiters here), it would send the following headers:</p>
<pre><code>POST /file1.txt HTTP/1.1
Host: localhost
Accept: */*
Content-Length: 6
Content-Type: application/x-www-form-urlencoded</code></pre>
<p>If you are not pleased with the default set of headers libcurl sends, the application has the power to add, change or remove headers in the HTTP request.</p>
<h3 id="add-a-header">Add a header</h3>
<p>To add a header that would not otherwise be in the request, add it with <code>CURLOPT_HTTPHEADER</code>. Suppose you want a header called <code>Name:</code> that contains <code>Mr. Smith</code>:</p>
<pre><code>struct curl_slist *list = NULL;
list = curl_slist_append(list, &quot;Name: Mr Smith&quot;);
curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
curl_easy_perform(curl);
curl_slist_free_all(list); /* free the list again */</code></pre>
<h3 id="change-a-header">Change a header</h3>
<p>If one of those default headers are not to your satisfaction you can alter them. Like if you think the default <code>Host:</code> header is wrong (even though it is derived from the URL you give libcurl), you can tell libcurl your own:</p>
<pre><code>struct curl_slist *list = NULL;
list = curl_slist_append(list, &quot;Host: Alternative&quot;);
curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
curl_easy_perform(curl);
curl_slist_free_all(list); /* free the list again */</code></pre>
<h3 id="remove-a-header">Remove a header</h3>
<p>When you think libcurl uses a header in a request that you really think it should not, you can easily tell it to just remove it from the request. Like if you want to take away the <code>Accept:</code> header. Just provide the header name with nothing to the right sight of the colon:</p>
<pre><code>struct curl_slist *list = NULL;
list = curl_slist_append(list, &quot;Accept:&quot;);
curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
curl_easy_perform(curl);
curl_slist_free_all(list); /* free the list again */</code></pre>
<h3 id="provide-a-header-without-contents">Provide a header without contents</h3>
<p>As you may then have noticed in the above sections, if you try to add a header with no contents on the right side of the colon, it will be treated as a removal instruction and it will instead completely inhibit that header from being sent. If you instead <em>truly</em> want to send a header with zero contents on the right side, you need to use a special marker. You must provide the header with a semicolon instead of a proper colon. Like <code>Header;</code>. So if you want to add a header to the outgoing HTTP request that is just <code>Moo:</code> with nothing following the colon, you could write it like:</p>
<pre><code>struct curl_slist *list = NULL;
list = curl_slist_append(list, &quot;Moo;&quot;);
curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
curl_easy_perform(curl);
curl_slist_free_all(list); /* free the list again */</code></pre>
<h2 id="referrer">Referrer</h2>
<p>The <code>Referer:</code> header (yes, it is misspelled) is a standard HTTP header that tells the server from which URL the user-agent was directed from when it arrived at the URL it now requests. It is a normal header so you can set it yourself with the <code>CURLOPT_HEADER</code> approach as shown above, or you can use the shortcut known as <code>CURLOPT_REFERER</code>. Like this:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_REFERER, &quot;https://example.com/fromhere/&quot;);
curl_easy_perform(curl);</code></pre>
<h3 id="automatic-referrer">Automatic referrer</h3>
<p>When libcurl is asked to follow redirects itself with the <code>CURLOPT_FOLLOWLOCATION</code> option, and you still want to have the <code>Referer:</code> header set to the correct previous URL from where it did the redirect, you can ask libcurl to set that by itself:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
curl_easy_setopt(curl, CURLOPT_AUTOREFERER, 1L);
curl_easy_setopt(curl, CURLOPT_URL, &quot;https://example.com/redirected.cgi&quot;);
curl_easy_perform(curl);</code></pre>
