<h2 id="verbose-mode">Verbose mode</h2>
<p>If your curl command does not execute or return what you expected it to, your first gut reaction should always be to run the command with the <code>-v / --verbose</code> option to get more information.</p>
<p>When verbose mode is enabled, curl gets more talkative and will explain and show a lot more of its doings. It will add informational tests and prefix them with ‘*’. For example, let’s see what curl might say when trying a simple HTTP example (saving the downloaded data in the file called ‘saved’):</p>
<pre><code>$ curl -v http://example.com -o saved
* Rebuilt URL to: http://example.com/</code></pre>
<p>Ok so we invoked curl with a URL that it considers incomplete so it helps us and it adds a trailing slash before it moves on.</p>
<pre><code>*   Trying 93.184.216.34...</code></pre>
<p>This tells us curl now tries to connect to this IP address. It means the name ‘example.com’ has been resolved to one or more addresses and this is the first (and possibly only) address curl will try to connect to.</p>
<pre><code>* Connected to example.com (93.184.216.34) port 80 (#0)</code></pre>
<p>It worked! curl connected to the site and here it explains how the name maps to the IP address and on which port it has connected to. The ‘(#0)’ part is which internal number curl has given this connection. If you try multiple URLs in the same command line you can see it use more connections or reuse connections, so the connection counter may increase or not increase depending on what curl decides it needs to do.</p>
<p>If we use an HTTPS:// URL instead of an HTTP one, there will also be a whole bunch of lines explaining how curl uses CA certs to verify the server’s certificate and some details from the server’s certificate, etc. Including which ciphers were selected and more TLS details.</p>
<p>In addition to the added information given from curl internals, the -v verbose mode will also make curl show all headers it sends and receives. For protocols without headers (like FTP, SMTP, POP3 and so on), we can consider commands and responses as headers and they will thus also be shown with -v.</p>
<p>If we then continue the output seen from the command above (but ignore the actual HTML response), curl will show:</p>
<pre><code>&gt; GET / HTTP/1.1
&gt; Host: example.com
&gt; User-Agent: curl/7.45.0
&gt; Accept: */*
&gt;</code></pre>
<p>This is the full HTTP request to the site. This request is how it looks in a default curl 7.45.0 installation and it may, of course, differ slightly between different releases and in particular it will change if you add command line options.</p>
<p>The last line of the HTTP request headers looks empty, and it is. It signals the separation between the headers and the body, and in this request there is no “body” to send.</p>
<p>Moving on and assuming everything goes according to plan, the sent request will get a corresponding response from the server and that HTTP response will start with a set of headers before the response body:</p>
<pre><code>&lt; HTTP/1.1 200 OK
&lt; Accept-Ranges: bytes
&lt; Cache-Control: max-age=604800
&lt; Content-Type: text/html
&lt; Date: Sat, 19 Dec 2015 22:01:03 GMT
&lt; Etag: &quot;359670651&quot;
&lt; Expires: Sat, 26 Dec 2015 22:01:03 GMT
&lt; Last-Modified: Fri, 09 Aug 2013 23:54:35 GMT
&lt; Server: ECS (ewr/15BD)
&lt; Vary: Accept-Encoding
&lt; X-Cache: HIT
&lt; x-ec-custom-error: 1
&lt; Content-Length: 1270
&lt;</code></pre>
<p>This may look mostly like mumbo jumbo to you, but this is normal set of HTTP headers—metadata—about the response. The first line’s “200” might be the most important piece of information in there and means “everything is fine”.</p>
<p>The last line of the received headers is, as you can see, empty, and that is the marker used for the HTTP protocol to signal the end of the headers.</p>
<p>After the headers comes the actual response body, the data payload. The regular -v verbose mode does not show that data but only displays</p>
<pre><code>{ [1270 bytes data]</code></pre>
<p>That 1270 bytes should then be in the ‘saved’ file. You can also see that there was a header named Content-Length: in the response that contained the exact file length (it will not always be present in responses).</p>
<h3 id="http2">HTTP/2</h3>
<p>When doing file transfers using version two of the HTTP protocol, HTTP/2, curl sends and receives <strong>compressed</strong> headers. So to display outgoing and incoming HTTP/2 headers in a readable and understandable way, curl will actually show the uncompressed versions in a style similar to how they appear with HTTP/1.1.</p>
<h3 id="silence">Silence</h3>
<p>The opposite of verbose is, of course, to make curl more silent. With the <code>-s</code> (or <code>--silent</code>) option you make curl switch off the progress meter and not output any error messages for when errors occur. It gets mute. It will still output the downloaded data you ask it to.</p>
<p>With silence activated, you can ask for it to still output the error message on failures by adding <code>-S</code> or <code>--show-error</code>.</p>
