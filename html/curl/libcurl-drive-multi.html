<h3 id="driving-with-the-multi-interface">Driving with the multi interface</h3>
<p>The name ‘multi’ is for multiple, as in multiple parallel transfers, all done in the same single thread. The multi API is non-blocking so it can also make sense to use it for single transfers.</p>
<p>The transfer is still set in an “easy” <code>CURL *</code> handle as described <a href="libcurl-easyhandle.md">above</a>, but with the multi interface you also need a multi <code>CURLM *</code> handle created and use that to drive all the individual transfers. The multi handle can “hold” one or many easy handles:</p>
<pre><code>CURLM *multi_handle = curl_multi_init();</code></pre>
<p>A multi handle can also get certain options set, which you do with <code>curl_multi_setopt()</code>, but in the simplest case you might not have anything to set there.</p>
<p>To drive a multi interface transfer, you first need to add all the individual easy handles that should be transferred to the multi handle. You can add them to the multi handle at any point and you can remove them again whenever you like. Removing an easy handle from a multi handle will, of course, remove the association and that particular transfer would stop immediately.</p>
<p>Adding an easy handle to the multi handle is easy:</p>
<pre><code>curl_multi_add_handle( multi_handle, easy_handle );</code></pre>
<p>Removing one is just as easily done:</p>
<pre><code>curl_multi_remove_handle( multi_handle, easy_handle );</code></pre>
<p>Having added the easy handles representing the transfers you want to perform, you write the transfer loop. With the multi interface, you do the looping so you can ask libcurl for a set of file descriptors and a timeout value and do the <code>select()</code> call yourself, or you can use the slightly simplified version which does that for us, with <code>curl_multi_wait</code>. The simplest loop would basically be this: (<em>note that a real application would check return codes</em>)</p>
<pre><code>int transfers_running;
do {
   curl_multi_wait ( multi_handle, NULL, 0, 1000, NULL);
   curl_multi_perform ( multi_handle, &amp;transfers_running );
} while (transfers_running);</code></pre>
<p>The fourth argument to <code>curl_multi_wait</code>, set to 1000 in the example above, is a timeout in milliseconds. It is the longest time the function will wait for any activity before it returns anyway. You do not want to lock up for too long before calling <code>curl_multi_perform</code> again as there are timeouts, progress callbacks and more that may loose precision if you do so.</p>
<p>To instead do select() on our own, we extract the file descriptors and timeout value from libcurl like this (<em>note that a real application would check return codes</em>):</p>
<pre><code>int transfers_running;
do {
  fd_set fdread;
  fd_set fdwrite;
  fd_set fdexcep;
  int maxfd = -1;
  long timeout;

  /* extract timeout value */
  curl_multi_timeout(multi_handle, &amp;timeout);
  if (timeout &lt; 0)
    timeout = 1000;

  /* convert to struct usable by select */
  timeout.tv_sec = timeout / 1000;
  timeout.tv_usec = (timeout % 1000) * 1000;

  FD_ZERO(&amp;fdread);
  FD_ZERO(&amp;fdwrite);
  FD_ZERO(&amp;fdexcep);

  /* get file descriptors from the transfers */
  mc = curl_multi_fdset(multi_handle, &amp;fdread, &amp;fdwrite, &amp;fdexcep, &amp;maxfd);

  if (maxfd == -1) {
    SHORT_SLEEP;
  }
  else
   select(maxfd+1, &amp;fdread, &amp;fdwrite, &amp;fdexcep, &amp;timeout);

  /* timeout or readable/writable sockets */
  curl_multi_perform(multi_handle, &amp;transfers_running);
} while ( transfers_running );</code></pre>
<p>Both these loops let you use one or more file descriptors of your own on which to wait, like if you read from your own sockets or a pipe or similar.</p>
<p>And again, you can add and remove easy handles to the multi handle at any point during the looping. Removing a handle mid-transfer will, of course, abort that transfer.</p>
<h2 id="when-is-a-single-transfer-done">When is a single transfer done?</h2>
<p>As the examples above show, a program can detect when an individual transfer completes by seeing that the <code>transfers_running</code> variable decreases.</p>
<p>It can also call <code>curl_multi_info_read()</code>, which will return a pointer to a struct (a “message”) if a transfer has ended and you can then find out the result of that transfer using that struct.</p>
<p>When you do multiple parallel transfers, more than one transfer can of course complete in the same <code>curl_multi_perform</code> invocation and then you might need more than one call to <code>curl_multi_info_read</code> to get info about each completed transfer.</p>
