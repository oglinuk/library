<h1 id="tls">TLS</h1>
<p>TLS stands for Transport Layer Security and is the name for the technology that was formerly called SSL. The term SSL has not really died though so these days both the terms TLS and SSL are often used interchangeably to describe the same thing.</p>
<p>TLS is a cryptographic security layer “on top” of TCP that makes the data tamper proof and guarantees server authenticity, based on strong public key cryptography and digital signatures.</p>
<h2 id="ciphers">Ciphers</h2>
<p>When curl connects to a TLS server, it negotiates how to speak the protocol and that negotiation involves several parameters and variables that both parties need to agree to. One of the parameters is which cryptography algorithms to use, the so called cipher. Over time, security researchers figure out flaws and weaknesses in existing ciphers and they are gradually phased out over time.</p>
<p>Using the verbose option, <code>-v</code>, you can get information about which cipher and TLS version are negotiated. By using the <code>--ciphers</code> option, you can change what cipher to prefer in the negotiation, but mind you, this is a power feature that takes knowledge to know how to use in ways that do not just make things worse.</p>
<h2 id="enable-tls">Enable TLS</h2>
<p>curl supports the TLS version of many protocols. HTTP has HTTPS, FTP has FTPS, LDAP has LDAPS, POP3 has POP3S, IMAP has IMAPS and SMTP has SMTPS.</p>
<p>If the server side supports it, you can use the TLS version of these protocols with curl.</p>
<p>There are two general approaches to do TLS with protocols. One of them is to speak TLS already from the first connection handshake while the other is to “upgrade” the connection from plain-text to TLS using protocol specific instructions.</p>
<p>With curl, if you explicitly specify the TLS version of the protocol (the one that has a name that ends with an ‘S’ character) in the URL, curl will try to connect with TLS from start, while if you specify the non-TLS version in the URL you can <em>usually</em> upgrade the connection to TLS-based with the <code>--ssl</code> option.</p>
<p>The support table looks like this:</p>
<table>
<thead>
<tr class="header">
<th>Clear</th>
<th>TLS version</th>
<th>–ssl</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>HTTP</td>
<td>HTTPS</td>
<td>no</td>
</tr>
<tr class="even">
<td>LDAP</td>
<td>LDAPS</td>
<td>no</td>
</tr>
<tr class="odd">
<td>FTP</td>
<td>FTPS</td>
<td><strong>yes</strong></td>
</tr>
<tr class="even">
<td>POP3</td>
<td>POP3S</td>
<td><strong>yes</strong></td>
</tr>
<tr class="odd">
<td>IMAP</td>
<td>IMAPS</td>
<td><strong>yes</strong></td>
</tr>
<tr class="even">
<td>SMTP</td>
<td>SMTPS</td>
<td><strong>yes</strong></td>
</tr>
</tbody>
</table>
<p>The protocols that <em>can</em> do <code>--ssl</code> all favor that method. Using <code>--ssl</code> means that curl will <em>attempt</em> to upgrade the connection to TLS but if that fails, it will still continue with the transfer using the plain-text version of the protocol. To make the <code>--ssl</code> option <strong>require</strong> TLS to continue, there’s instead the <code>--ssl-reqd</code> option which will make the transfer fail if curl cannot successfully negotiate TLS.</p>
<p>Require TLS security for your FTP transfer:</p>
<pre><code>curl --ssl-reqd ftp://ftp.example.com/file.txt</code></pre>
<p>Suggest TLS to be used for your FTP transfer:</p>
<pre><code>curl --ssl ftp://ftp.example.com/file.txt</code></pre>
<p>Connecting directly with TLS (to HTTPS://, LDAPS://, FTPS:// etc) means that TLS is mandatory and curl will return an error if TLS is not negotiated.</p>
<p>Get a file over HTTPS:</p>
<pre><code>curl https://www.example.com/</code></pre>
<h2 id="ssl-and-tls-versions">SSL and TLS versions</h2>
<p>SSL was invented in the mid 90s and has developed ever since. SSL version 2 was the first widespread version used on the Internet but that was deemed insecure already a long time ago. SSL version 3 took over from there, and it too has been deemed not safe enough for use.</p>
<p>TLS version 1.0 was the first “standard”. RFC 2246 was published 1999. TLS 1.1 came out in 2006, further improving security, followed by TLS 1.2 in 2008. TLS 1.2 came to be the gold standard for TLS for a decade.</p>
<p>TLS 1.3 (RFC 8446) was finalized and published as a standard by the IETF in August 2018. This is the most secure and fastest TLS version as of date. It is however so new that a lot of software, tools and libraries do not yet support it.</p>
<p>curl is designed to use a “safe version” of SSL/TLS by default. It means that it will not negotiate SSLv2 or SSLv3 unless specifically told to, and in fact several TLS libraries no longer provide support for those protocols so in many cases curl is not even able to speak those protocol versions unless you make a serious effort.</p>
<table>
<thead>
<tr class="header">
<th>Option</th>
<th>Use</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>–sslv2</td>
<td>SSL version 2</td>
</tr>
<tr class="even">
<td>–sslv3</td>
<td>SSL version 3</td>
</tr>
<tr class="odd">
<td>–tlsv1</td>
<td>TLS &gt;= version 1.0</td>
</tr>
<tr class="even">
<td>–tlsv1.0</td>
<td>TLS &gt;= version 1.0</td>
</tr>
<tr class="odd">
<td>–tlsv1.1</td>
<td>TLS &gt;= version 1.1</td>
</tr>
<tr class="even">
<td>–tlsv1.2</td>
<td>TLS &gt;= version 1.2</td>
</tr>
<tr class="odd">
<td>–tlsv1.3</td>
<td>TLS &gt;= version 1.3</td>
</tr>
</tbody>
</table>
<h2 id="verifying-server-certificates">Verifying server certificates</h2>
<p>Having a secure connection to a server is not worth a lot if you cannot also be certain that you are communicating with the <strong>correct</strong> host. If we do not know that, we could just as well be talking with an impostor that just <em>appears</em> to be who we think it is.</p>
<p>To check that it communicates with the right TLS server, curl uses a set of locally stored CA certificates to verify the signature of the server’s certificate. All servers provide a certificate to the client as part of the TLS handshake and all public TLS-using servers have acquired that certificate from an established Certificate Authority.</p>
<p>After some applied crypto magic, curl knows that the server is in fact the correct one that acquired that certificate for the host name that curl used to connect to it. Failing to verify the server’s certificate is a TLS handshake failure and curl exits with an error.</p>
<p>In rare circumstances, you may decide that you still want to communicate with a TLS server even if the certificate verification fails. You then accept the fact that your communication may be subject to Man-In-The-Middle attacks. You lower your guards with the <code>-k</code> or <code>--insecure</code> option.</p>
<h2 id="ca-store">CA store</h2>
<p>curl needs a “CA store”, a collection of CA certificates, to verify the TLS server it talks to.</p>
<p>If curl is built to use a TLS library that is “native” to your platform, chances are that library will use the native CA store as well. If not, curl has to either have been built to know where the local CA store is, or users need to provide a path to the CA store when curl is invoked.</p>
<p>You can point out a specific CA bundle to use in the TLS handshake with the <code>--cacert</code> command line option. That bundle needs to be in PEM format. You can also set the environment variable <code>CURL_CA_BUNDLE</code> to the full path.</p>
<h3 id="ca-store-on-windows">CA store on windows</h3>
<p>curl built on windows that is not using the native TLS library (Schannel), have an extra sequence for how the CA store can be found and used.</p>
<p>curl will search for a CA cert file named “curl-ca-bundle.crt” in these directories and in this order:</p>
<ol class="incremental" type="1">
<li>application’s directory</li>
<li>current working directory</li>
<li>Windows System directory (e.g. <code>C:\windows\system32</code>)</li>
<li>Windows Directory (e.g. <code>C:\windows</code>)</li>
<li>all directories along <code>%PATH%</code></li>
</ol>
<h2 id="certificate-pinning">Certificate pinning</h2>
<p>TLS certificate pinning is a way to verify that the public key used to sign the servers certificate has not changed. It is “pinned”.</p>
<p>When negotiating a TLS or SSL connection, the server sends a certificate indicating its identity. A public key is extracted from this certificate and if it does not exactly match the public key provided to this option, curl will abort the connection before sending or receiving any data.</p>
<p>You tell curl a file name to read the sha256 value from, or you specify the base64 encoded hash directly in the command line with a <code>sha256//</code> prefix. You can specify one or more hashes like that, separated with semicolons (;).</p>
<pre><code>curl --pinnedpubkey &quot;sha256//83d34tasd3rt...&quot; https://example.com/</code></pre>
<p>This feature is not supported by all TLS backends.</p>
<h2 id="ocsp-stapling">OCSP stapling</h2>
<p>This uses the TLS extension called Certificate Status Request to ask the server to provide a fresh “proof” from the CA in the handshake, that the certificate that it returns is still valid. This is a way to make really sure the server’s certificate has not been revoked.</p>
<p>If the server does not support this extension, the test will fail and curl returns an error. And it is still far too common that servers do not support this.</p>
<p>Ask for the handshake to use the status request like this:</p>
<pre><code>curl --cert-status https://example.com/</code></pre>
<p>This feature is only supported by the OpenSSL, GnuTLS and NSS backends.</p>
<h2 id="client-certificates">Client certificates</h2>
<p>TLS client certificates are a way for clients to cryptographically prove to servers that they are truly the right peer. A command line that uses a client certificate specifies the certificate and the corresponding key, and they are then passed on the TLS handshake with the server.</p>
<p>You need to have your client certificate already stored in a file when doing this and you should supposedly have gotten it from the right instance via a different channel previously.</p>
<p>The key is typically protected by a password that you need to provide or get prompted for interactively.</p>
<p>curl offers options to let you specify a single file that is both the client certificate and the private key concatenated using <code>--cert</code>, or you can specify the key file independently with <code>--key</code>:</p>
<pre><code>curl --cert mycert:mypassword https://example.com
curl --cert mycert:mypassword --key mykey https://example.com</code></pre>
<p>For some TLS backends you can also pass in the key and certificate using different types:</p>
<pre><code>curl --cert mycert:mypassword --cert-type PEM \
     --key mykey --key-type PEM https://example.com</code></pre>
<h2 id="tls-auth">TLS auth</h2>
<p>TLS connections offer a (rarely used) feature called Secure Remote Passwords. Using this, you authenticate the connection for the server using a name and password and the command line flags for this are <code>--tlsuser &lt;name&gt;</code> and <code>--tlspassword &lt;secret&gt;</code>. Like this:</p>
<pre><code>curl --tlsuser daniel --tlspassword secret https://example.com</code></pre>
<h2 id="different-tls-backends">Different TLS backends</h2>
<p>When curl is built, it gets told to use a specific TLS library. That TLS library is the engine that provides curl with the powers to speak TLS over the wire. We often refer to them as different “backends” as they can be seen as different plugglable pieces into the curl machine. curl can be built to be able to use one or more of these backends.</p>
<p>Sometimes features and behaviors differ slightly when curl is built with different TLS backends, but the developers work hard on making those differences as small and unnoticable as possible.</p>
<p>Showing the curl version information with <code>curl --version</code> will always include the TLS library and version in the first line of output.</p>
<h2 id="multiple-tls-backends">Multiple TLS backends</h2>
<p>When curl is built with <em>multiple</em> TLS backends, it be told which one to use each time it is started. It is always built to use a specific one by default unless one is asked for.</p>
<p>If you invoke <code>curl --version</code> for a curl with multiple backends it will mention <code>MultiSSL</code> as a feature in the last line. The first line will then include all the supported TLS backends with all but the default one within parentheses.</p>
<p>To set a specific one to get used, set the environment variaable <code>CURL_SSL_BACKEND</code> to the name of it!</p>
