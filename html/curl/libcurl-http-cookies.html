<h1 id="cookies-with-libcurl">Cookies with libcurl</h1>
<p>By default and by design, libcurl makes transfers as basic as possible and features need to be enabled to get used. One such feature is HTTP cookies, more known as just plain and simply “cookies”.</p>
<p>Cookies are name/value pairs sent by the server (using a <code>Set-Cookie:</code> header) to be stored in the client, and are then supposed to get sent back again in requests that matches the host and path requirements that were specified along with the cookie when it came from the server (using the <code>Cookie:</code> header). On the modern web of today, sites are known to sometimes use large numbers of cookies.</p>
<h2 id="cookie-engine">Cookie engine</h2>
<p>When you enable the “cookie engine” for a specific easy handle, it means that it will record incoming cookies, store them in the in-memory “cookie store” that is associated with the easy handle and subsequently send the proper ones back if an HTTP request is made that matches.</p>
<p>There are two ways to switch on the cookie engine:</p>
<h3 id="enable-cookie-engine-with-reading">Enable cookie engine with reading</h3>
<p>Ask libcurl to import cookies into the easy handle from a given file name with the <code>CURLOPT_COOKIEFILE</code> option:</p>
<pre><code>curl_easy_setopt(easy, CURLOPT_COOKIEFILE, &quot;cookies.txt&quot;);</code></pre>
<p>A common trick is to just specify a non-existing file name or plain "" to have it just activate the cookie engine with a blank cookie store to start with.</p>
<p>This option can be set multiple times and then each of the given files will be read.</p>
<h3 id="enable-cookie-engine-with-writing">Enable cookie engine with writing</h3>
<p>Ask for received cookies to get stored in a file with the <code>CURLOPT_COOKIEJAR</code> option:</p>
<pre><code>curl_easy_setopt(easy, CURLOPT_COOKIEJAR, &quot;cookies.txt&quot;);</code></pre>
<p>when the easy handle is closed later with <code>curl_easy_cleanup()</code>, all known cookies will be written to the given file. The file format is the well-known “Netscape cookie file” format that browsers also once used.</p>
<h2 id="setting-custom-cookies">Setting custom cookies</h2>
<p>A simpler and more direct way to just pass on a set of specific cookies in a request that does not add any cookies to the cookie store and doesn’t even activate the cookie engine, is to set the set with `CURLOPT_COOKIE:’:</p>
<pre><code>curl_easy_setopt(easy, CURLOPT_COOKIE, &quot;name=daniel; present=yes;&quot;);</code></pre>
<p>The string you set there is the raw string that would be sent in the HTTP request and should be in the format of repeated sequences of <code>NAME=VALUE;</code> - including the semicolon separator.</p>
<h2 id="import-export">Import export</h2>
<p>The cookie in-memory store can hold a bunch of cookies, and libcurl offers very powerful ways for an application to play with them. You can set new cookies, you can replace an existing cookie and you can extract existing cookies.</p>
<h3 id="add-a-cookie-to-the-cookie-store">Add a cookie to the cookie store</h3>
<p>Add a new cookie to the cookie store by simply passing it into curl with <code>CURLOPT_COOKIELIST</code> with a new cookie. The format of the input is a single line in the cookie file format, or formatted as a <code>Set-Cookie:</code> response header, but we recommend the cookie file style:</p>
<pre><code>#define SEP  &quot;\\t&quot;  /* Tab separates the fields */

char *my_cookie =
  &quot;example.com&quot;    /* Hostname */
  SEP &quot;FALSE&quot;      /* Include subdomains */
  SEP &quot;/&quot;          /* Path */
  SEP &quot;FALSE&quot;      /* Secure */
  SEP &quot;0&quot;          /* Expiry in epoch time format. 0 == Session */
  SEP &quot;foo&quot;        /* Name */
  SEP &quot;bar&quot;;       /* Value */

curl_easy_setopt(curl, CURLOPT_COOKIELIST, my_cookie);</code></pre>
<p>If that given cookie would match an already existing cookie (with the same domain and path, etc.), it would overwrite the old one with the new contents.</p>
<h3 id="get-all-cookies-from-the-cookie-store">Get all cookies from the cookie store</h3>
<p>Sometimes writing the cookie file when you close the handle is not enough and then your application can opt to extract all the currently known cookies from the store like this:</p>
<pre><code>struct curl_slist *cookies
curl_easy_getinfo(easy, CURLINFO_COOKIELIST, &amp;cookies);</code></pre>
<p>This returns a pointer to a linked list of cookies, and each cookie is (again) specified as a single line of the cookie file format. The list is allocated for you, so do not forget to call <code>curl_slist_free_all</code> when the application is done with the information.</p>
<h3 id="cookie-store-commands">Cookie store commands</h3>
<p>If setting and extracting cookies is not enough, you can also interfere with the cookie store in more ways:</p>
<p>Wipe the entire in-memory storage clean with:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_COOKIELIST, &quot;ALL&quot;);</code></pre>
<p>Erase all session cookies (cookies without expiry date) from memory:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_COOKIELIST, &quot;SESS&quot;);</code></pre>
<p>Force a write of all cookies to the file name previously specified with <code>CURLOPT_COOKIEJAR</code>:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_COOKIELIST, &quot;FLUSH&quot;);</code></pre>
<p>Force a reload of cookies from the file name previously specified with <code>CURLOPT_COOKIEFILE</code>:</p>
<pre><code>curl_easy_setopt(curl, CURLOPT_COOKIELIST, &quot;RELOAD&quot;);</code></pre>
<h2 id="cookie-file-format">Cookie file format</h2>
<p>The cookie file format is text based and stores one cookie per line. Lines that start with <code>#</code> are treated as comments.</p>
<p>Each line that each specifies a single cookie consists of seven text fields separated with TAB characters.</p>
<table>
<thead>
<tr class="header">
<th>Field</th>
<th>Example</th>
<th>Meaning</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>0</td>
<td>example.com</td>
<td>Domain name</td>
</tr>
<tr class="even">
<td>1</td>
<td>FALSE</td>
<td>Include subdomains boolean</td>
</tr>
<tr class="odd">
<td>2</td>
<td>/foobar/</td>
<td>Path</td>
</tr>
<tr class="even">
<td>3</td>
<td>FALSE</td>
<td>Set over a secure transport</td>
</tr>
<tr class="odd">
<td>4</td>
<td>1462299217</td>
<td>Expires at – seconds since Jan 1st 1970, or 0</td>
</tr>
<tr class="even">
<td>5</td>
<td>person</td>
<td>Name of the cookie</td>
</tr>
<tr class="odd">
<td>6</td>
<td>daniel</td>
<td>Value of the cookie</td>
</tr>
</tbody>
</table>
