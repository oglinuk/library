<h1 id="modify-the-http-request">Modify the HTTP request</h1>
<p>As described earlier, each HTTP transfer starts with curl sending a HTTP request. That request consists of a request line and a number of request headers, and this chapter details how you can modify all of those.</p>
<h2 id="request-method">Request method</h2>
<p>The first line of the request includes the <em>method</em> - sometimes also referred to as “the verb”. When doing a simple GET request as this command line would do:</p>
<pre><code>curl http://example.com/file</code></pre>
<p>…the initial request line looks like this:</p>
<pre><code>GET /file HTTP/1.1</code></pre>
<p>You can tell curl to change the method into something else by using the <code>-X</code> or <code>--request</code> command-line options followed by the actual method name. You can, for example, send a <code>DELETE</code> instead of <code>GET</code> like this:</p>
<pre><code>curl http://example.com/file -X DELETE</code></pre>
<p>This command-line option only changes the text in the outgoing request, it does not change any behavior. This is particularly important if you, for example, ask curl to send a HEAD with <code>-X</code>, as HEAD is specified to send all the headers a GET response would get but <em>never</em> send a response body, even if the headers otherwise imply that one would come. So, adding <code>-X HEAD</code> to a command line that would otherwise do a GET will cause curl to hang, waiting for a response body that will not come.</p>
<p>When asking curl to perform HTTP transfers, it will pick the correct method based on the option so you should only rarely have to explicitly ask for it with <code>-X</code>. It should also be noted that when curl follows redirects like asked to with <code>-L</code>, the request method set with <code>-X</code> will be sent even on the subsequent redirects.</p>
<h2 id="request-target">Request target</h2>
<p>In the example above, you can see how the path section of the URL gets turned into <code>/file</code> in the request line. That is called the “request target”. That’s the resource this request will interact with. Normally this request target is extracted from the URL and then used in the request and as a user you do not need to think about it.</p>
<p>In some rare circumstances, user may want to go creative and change this request target in ways that the URL does not really allow. For example, the HTTP OPTIONS method has a specially define request target for magic that concerns <em>the server</em> and not a specific path, and it uses <code>*</code> for that. Yes, a single asterisk. There’s no way to specify a URL for this, so if you want to pass a single asterisk in the request target to a server, like for OPTIONS, you have to do it like this:</p>
<pre><code>curl -X OPTIONS --request-target &quot;*&quot; http://example.com/</code></pre>
<h2 id="anchors-or-fragments">Anchors or fragments</h2>
<p>A URL may contain an anchor, also known as a fragment, which is written with a pound sign and string at the end of the URL. Like for example <code>http://example.com/foo.html#here-it-is</code>. That fragment part, everything from the pound/hash sign to the end of the URL, is only intend for local use and will not be sent over the network. curl will simply strip that data off and discard it.</p>
<h2 id="customize-headers">Customize headers</h2>
<p>In a HTTP request, after the initial request-line, there will typically follow a number of request headers. That’s a set of <code>name: value</code> pairs that ends with a blank line that separates the headers from the following request body (that sometimes is empty).</p>
<p>curl will by default and on its own account pass a few headers in requests, like for example <code>Host:</code>, <code>Accept:</code>, <code>User-Agent:</code> and a few others that may depend on what the user asks curl to do.</p>
<p>All headers set by curl itself can be overridden, replaced if you will, by the user. You just then tell curl’s <code>-H</code> or <code>--header</code> the new header to use and it will then replace the internal one if the header field matches one of those headers, or it will add the specified header to the list of headers to send in the request.</p>
<p>To change the <code>Host:</code> header, do this:</p>
<pre><code>curl -H &quot;Host: test.example&quot; http://example.com/</code></pre>
<p>To add a <code>Elevator: floor-9</code> header, do this:</p>
<pre><code>curl -H &quot;Elevator: floor-9&quot; http://example.com/</code></pre>
<p>If you just want to delete an internally generated header, just give it to curl without a value, just nothing on the right side of the colon.</p>
<p>To switch off the <code>User-Agent:</code> header, do this:</p>
<pre><code>curl -H &quot;User-Agent:&quot; http://example.com/</code></pre>
<p>Finally, if you then truly want to add a header with no contents on the right side of the colon (which is a rare thing), the magic marker for that is to instead end the header field name with a <em>semicolon</em>. Like this:</p>
<pre><code>curl -H &quot;Empty;&quot; http://example.com</code></pre>
<h2 id="referer">Referer</h2>
<p>When a user clicks on a link on a web page and the browser takes the user away to the next URL, it will send the new URL a “referer” header in the new request telling it where it came from. That is the referer header. And yes, referer is misspelled but that’s how it is supposed to be!</p>
<p>With curl you set the referer header with <code>-e</code> or <code>--referer</code>, like this:</p>
<pre><code>curl --referer http://comes-from.example.com https://www.example.com/</code></pre>
<h2 id="user-agent">User-agent</h2>
<p>The User-Agent is a header that each client can set in the request to inform the server which user-agent it is. Sometimes servers will look at this header and determine how to act based on its contents.</p>
<p>The default header value is ‘curl/[version]’, as in <code>User-Agent: curl/7.54.1</code> for curl version 7.54.1.</p>
<p>You can set any value you like, using the option <code>-A</code> or <code>--user-agent</code> plus the string to use or, as it’s just a header, <code>-H "User-Agent: foobar/2000"</code>.</p>
<p>As comparison, a recent test version of Firefox on a Linux machine sent this User-Agent header:</p>
<p><code>User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0</code></p>
<h2 id="time-conditions">Time conditions</h2>
<p>HTTP allows for “conditional requests”. They are requests that contain a condition in the sense that it asks the server to only deliver a response body if the associated condition evaluates true.</p>
<p>A useful condition is time. For example, ask the server to only deliver a response if the resource has been modified after a particular time:</p>
<pre><code>curl --time-cond &quot;1 Jul 2011&quot; https://www.example.org/file.html</code></pre>
<p>curl can also reverse the condition. Only get the file if it is <em>older</em> than the given date by prefixing the date with a dash:</p>
<pre><code>curl --time-cond &quot;-1 Jul 2011&quot; https://www.example.org/file.html</code></pre>
<p>The date parser is liberal and will accept most formats you can write the date, and you can of course also specify it complete with a time:</p>
<pre><code>curl --time-cond &quot;Sun, 12 Sep 2004 15:05:58 -0700&quot; https://www.example.org/file.html</code></pre>
<p>curl can also get the time stamp off a local file as a shortcut. No need to download the file again if it has not changed on the server, right? If the string does not match a time or date, curl checks if there’s a file named like that, and if so gets the time from it’s modification time.</p>
<pre><code>curl --time-cond file https://www.example.org/file.html -o file</code></pre>
