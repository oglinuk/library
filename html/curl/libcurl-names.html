<h1 id="name-resolving">Name resolving</h1>
<p>Most transfers libcurl can do involves a name that first needs to be translated to an Internet address. That’s “name resolving”. Using a numerical IP address directly in the URL usually avoids the name resolve phase, but in many cases it is not easy to manually replace the name with the IP address.</p>
<p>libcurl tries hard to <a href="libcurl-connectionreuse.md">re-use an existing connection</a> rather than to create a new one. The function that checks for an existing connection to use is based purely on the name and is performed before any name resolving is attempted. That’s one of the reasons the re-use is so much faster. A transfer using a reused connection will not resolve the host name again.</p>
<p>If no connection can be reused, libcurl resolves the host name to the set of addresses it resolves to. Typically this means asking for both IPv4 and IPv6 addresses and there may be a whole set of those returned to libcurl. That set of addresses is then tried until one works, or it returns failure.</p>
<p>An application can force libcurl to use only an IPv4 or IPv6 resolved address by setting <code>CURLOPT_IPRESOLVE</code> to the preferred value. For example, ask to only use IPv6 addresses:</p>
<pre><code>curl_easy_setopt(easy, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V6);</code></pre>
<h2 id="name-resolver-backends">Name resolver backends</h2>
<p>libcurl can be built to do name resolves in one out of these three different ways and depending on which backend way that is used, it gets a slightly different feature set and sometimes modified behavior.</p>
<ol class="incremental" type="1">
<li><p>The default backend is invoking the “normal” libc resolver functions in a new helper-thread, so that it can still do fine-grained timeouts if wanted and there will be no blocking calls involved.</p></li>
<li><p>On older systems, libcurl uses the standard synchronous name resolver functions. They unfortunately make all transfers within a multi handle block during its operation and it is much harder to time out nicely.</p></li>
<li><p>There’s also support for resolving with the c-ares third party library, which supports asynchronous name resolving without the use of threads. This scales better to huge number of parallel transfers but it is not always 100% compatible with the native name resolver functionality.</p></li>
</ol>
<h3 id="dns-over-https">DNS over HTTPS</h3>
<p>Independently of what resolver backend that libcurl is built to use, since 7.62.0 it also provides a way for the user to ask a specific DoH (DNS over HTTPS) server for the address of a name. This will avoid using the normal, native resolver method and server and instead asks a dedicated separate one.</p>
<p>A DoH server is specified as a full URL with the <code>CURLOPT_DOH_URL</code> option like this:</p>
<pre><code>curl_easy_setopt(easy, CURLOPT_DOH_URL, &quot;https://example.com/doh&quot;);</code></pre>
<p>The URL passed to this option <em>must</em> be using https:// and it is generally recommended that you have HTTP/2 support enabled so that libcurl can perform multiple DoH requests multiplexed over the connection to the DoH server.</p>
<h2 id="caching">Caching</h2>
<p>When a name has been resolved, the result will be put in libcurl’s in-memory cache so that subsequent resolves of the same name will be near instant for as long the name is kept in the DNS cache. By default, each entry is kept in the cache for 60 seconds, but that value can be changed with <code>CURLOPT_DNS_CACHE_TIMEOUT</code>.</p>
<p>The DNS cache is kept within the easy handle when <code>curl_easy_perform</code> is used, or within the multi handle when the multi interface is used. It can also be made shared between multiple easy handles using the <a href="libcurl-sharing.md">share interface</a>.</p>
<h2 id="custom-addresses-for-hosts">Custom addresses for hosts</h2>
<p>Sometimes it is handy to provide “fake” addresses to real host names so that libcurl will connect to a different address instead of one an actual name resolve would suggest.</p>
<p>With the help of the <a href="https://curl.haxx.se/libcurl/c/CURLOPT_RESOLVE.html">CURLOPT_RESOLVE</a> option, an application can pre-populate libcurl’s DNS cache with a custom address for a given host name and port number.</p>
<p>To make libcurl connect to 127.0.0.1 when example.com on port 443 is requested, an application can do:</p>
<pre><code>struct curl_slist *dns;
dns = curl_slist_append(NULL, &quot;example.com:443:127.0.0.1&quot;);
curl_easy_setopt(curl, CURLOPT_RESOLVE, dns);</code></pre>
<p>Since this puts the “fake” address into the DNS cache, it will work even when following redirects etc.</p>
<h2 id="name-server-options">Name server options</h2>
<p>For libcurl built to use c-ares, there’s a few options available that offer fine-grained control of what DNS servers to use and how. This is limited to c-ares build purely because these are powers that are not available when the standard system calls for name resolving are used.</p>
<ul class="incremental">
<li><p>With <code>CURLOPT_DNS_SERVERS</code>, the application can select to use a set of dedicated DNS servers.</p></li>
<li><p>With <code>CURLOPT_DNS_INTERFACE</code> it can tell libcurl which network interface to speak DNS over instead of the default one.</p></li>
<li><p>With <code>CURLOPT_DNS_LOCAL_IP4</code> and <code>CURLOPT_DNS_LOCAL_IP6</code>, the application can specify which specific network addresses to bind DNS resolves to.</p></li>
</ul>
<h2 id="no-global-dns-cache">No global DNS cache</h2>
<p>The option called <code>CURLOPT_DNS_USE_GLOBAL_CACHE</code> once told curl to use a global DNS cache. This functionality has been removed since 7.65.0, so while this option still exists it does nothing.</p>
