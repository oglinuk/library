<h1 id="exit-status">Exit status</h1>
<p>A lot of effort has gone into the project to make curl return a usable exit code when something goes wrong and it will always return 0 (zero) when the operation went as planned.</p>
<p>If you write a shell script or batch file that invokes curl, you can always check the return code to detect problems in the invoked command. Below, you will find a list of return codes as of the time of this writing. Over time we tend to slowly add new ones so if you get a code back not listed here, please refer to more updated curl documentation for aid.</p>
<p>A basic Unix shell script could look like something like this:</p>
<pre><code>#!/bin/sh
curl http://example.com
res=$?
if test &quot;$res&quot; != &quot;0&quot;; then
   echo &quot;the curl command failed with: $res&quot;
fi</code></pre>
<h2 id="available-exit-codes">Available exit codes</h2>
<ol class="incremental" type="1">
<li><p>Unsupported protocol. This build of curl has no support for this protocol. Usually this happens because the URL was misspelled to use a scheme part that either has a space in front of it or spells “http” like “htpt” or similar. Another common mistake is that you use a libcurl installation that was built with one or more protocols disabled and you now ask libcurl to use one of those protocols that were disabled in the build.</p></li>
<li><p>Failed to initialize. This is mostly an internal error or a problem with the libcurl installation or system libcurl runs in.</p></li>
<li><p>URL malformed. The syntax was not correct. This happens when you mistype a URL so that it ends up wrong, or in rare situations you are using a URL that is accepted by another tool that curl does not support only because there is no universal URL standard that everyone adheres to.</p></li>
<li><p>A feature or option that was needed to perform the desired request was not enabled or was explicitly disabled at build-time. To make curl able to do this, you probably need another build of libcurl.</p></li>
<li><p>Couldn’t resolve proxy. The address of the given proxy host could not be resolved. Either the given proxy name is just wrong, or the DNS server is misbehaving and doesn’t know about this name when it should or perhaps even the system you run curl on is misconfigured so that it does not find/use the correct DNS server.</p></li>
<li><p>Couldn’t resolve host. The given remote host’s address was not resolved. The address of the given server could not be resolved. Either the given host name is just wrong, or the DNS server is misbehaving and does not know about this name when it should or perhaps even the system you run curl on is misconfigured so that it does not find/use the correct DNS server.</p></li>
<li><p>Failed to connect to host. curl managed to get an IP address to the machine and it tried to setup a TCP connection to the host but failed. This can be because you have specified the wrong port number, entered the wrong host name, the wrong protocol or perhaps because there is a firewall or another network equipment in between that blocks the traffic from getting through.</p></li>
<li><p>Unknown FTP server response. The server sent data curl could not parse. This is either because of a bug in curl, a bug in the server or because the server is using an FTP protocol extension that curl does not support. The only real work-around for this is to tweak curl options to try it to use other FTP commands that perhaps will not get this unknown server response back.</p></li>
<li><p>FTP access denied. The server denied login or denied access to the particular resource or directory you wanted to reach. Most often you tried to change to a directory that does not exist on the server. The directory of course is what you specify in the URL.</p></li>
<li><p>FTP accept failed. While waiting for the server to connect back when an active FTP session is used, an error code was sent over the control connection or similar.</p></li>
<li><p>FTP weird PASS reply. Curl could not parse the reply sent to the PASS request. PASS in the command curl sends the password to the server with, and even anonymous connections to FTP server actually sends a password - a fixed anonymous string. Getting a response back from this command that curl does not understand is a strong indication that this isn’t an FTP server at all or that the server is badly broken.</p></li>
<li>During an active FTP session (PORT is used) while waiting for the server to connect, the timeout expired. It took too long for the server to get back. This is usually a sign that something is preventing the server from reaching curl successfully. Like a firewall or other network arrangements. .</li>
<li><p>Unknown response to FTP PASV command, Curl could not parse the reply sent to the PASV request. This is a strange server. PASV is used to setup the second data transfer connection in passive mode, see the <a href="ftp-twoconnections.md">FTP uses two connections</a> section for more on that. You might be able to work-around this problem by using PORT instead, with the <code>--ftp-port</code> option.</p></li>
<li><p>Unknown FTP 227 format. Curl could not parse the 227-line the server sent. This is most certainly a broken server. A 227 is the FTP server’s response when sending back information on how curl should connect back to it in passive mode. You might be able to work-around this problem by using PORT instead, with the <code>--ftp-port</code> option.</p></li>
<li><p>FTP can’t get host. Couldn’t use the host IP address we got in the 227-line. This is most likely an internal error.</p></li>
<li><p>HTTP/2 error. A problem was detected in the HTTP2 framing layer. This is somewhat generic and can be one out of several problems, see the error message for details.</p></li>
<li><p>FTP couldn’t set binary. Couldn’t change transfer method to binary. This server is broken. curl needs to set the transfer to the correct mode before it is started as otherwise the transfer cannot work.</p></li>
<li><p>Partial file. Only a part of the file was transferred. When the transfer is considered complete, curl will verify that it actually received the same amount of data that it was told before-hand that it was going to get. If the two numbers do not match, this is the error code. It could mean that curl got fewer bytes than advertised or that it got more. curl itself cannot know which number that is wrong or which is correct. If any.</p></li>
<li><p>FTP couldn’t download/access the given file. The RETR (or similar) command failed. curl got an error from the server when trying to download the file.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Quote error. A quote command returned an error from the server. curl allows several different ways to send custom commands to a IMAP, POP3, SMTP or FTP server and features a generic check that the commands work. When any of the individually issued commands fails, this is exit status is returned. The advice is generally to watch the headers in the FTP communication to better understand exactly what failed and how.</p></li>
<li><p>HTTP page not retrieved. The requested url was not found or returned another error with the HTTP error code being 400 or above. This return code only appears if <code>-f, --fail</code> is used.</p></li>
<li><p>Write error. Curl could not write data to a local filesystem or similar. curl receives data chunk by chunk from the network and it stores it like at (or writes it to stdout), one piece at a time. If that write action gets an error, this is the exit status.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Upload failed. The server refused to accept or store the file that curl tried to send to it. This is usually due to wrong access rights on the server but can also happen due to out of disk space or other resource constraints. This error can happen for many protocols.</p></li>
<li><p>Read error. Various reading problems. The inverse to exit status 23. When curl sends data to a server, it reads data chunk by chunk from a local file or stdin or similar, and if that reading fails in some way this is the exit status curl will return.</p></li>
<li><p>Out of memory. A memory allocation request failed. curl needed to allocate more memory than what the system was willing to give it and curl had to exit. Try using smaller files or make sure that curl gets more memory to work with.</p></li>
<li><p>Operation timeout. The specified time-out period was reached according to the conditions. curl offers several <a href="usingcurl-timeouts.md">timeouts</a>, and this exit code tells one of those timeout limits were reached. Extend the timeout or try changing something else that allows curl to finish its operation faster. Often, this happens due to network and remote server situations that you cannot affect locally.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>FTP PORT failed. The PORT command failed. Not all FTP servers support the PORT command; try doing a transfer using PASV instead. The PORT command is used to ask the server to create the data connection by <em>connecting back</em> to curl. See also the <a href="ftp-twoconnections.md">FTP uses two connections</a> section.</p></li>
<li><p>FTP couldn’t use REST. The REST command failed. This command is used for resumed FTP transfers. curl needs to issue the REST command to do range or resumed transfers. The server is broken, try the same operation without range/resume as a crude work-around.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>HTTP range error. The range request did not work. Resumed HTTP requests are not necessary acknowledged or supported, so this exit code signals that for this resource on this server, there can be no range or resumed transfers.</p></li>
<li><p>HTTP post error. Internal post-request generation error. If you get this error, please report the exact circumstances to the curl project.</p></li>
<li><p>A TLS/SSL connect error. The SSL handshake failed. The SSL handshake can fail due to numerous different reasons so the error message may offer some additional clues. Maybe the parties could not agree to a SSL/TLS version, an agreeable cipher suite or similar.</p></li>
<li><p>Bad download resume. Could not continue an earlier aborted download. When asking to resume a transfer that then ends up not possible to do, this error can get returned. For FILE, FTP or SFTP.</p></li>
<li><p>Couldn’t read the given file when using the FILE:// scheme. Failed to open the file. The file could be non-existing or is it a permission problem perhaps?</p></li>
<li><p>LDAP cannot bind. LDAP “bind” operation failed, which is a necessary step in the LDAP operation and thus this means the LDAP query could not be performed. This might happen because of wrong username or password, or for other reasons.</p></li>
<li><p>LDAP search failed. The given search terms caused the LDAP search to return an error.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Aborted by callback. An application told libcurl to abort the operation. This error code is not generally made visible to users and not to users of the curl tool.</p></li>
<li><p>Bad function argument. A function was called with a bad parameter - this return code is present to help application authors to understand why libcurl cannot perform certain actions and should never be return by the curl tool. Please file a bug report to the curl project if this happens to you.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Interface error. A specified outgoing network interface could not be used. curl will typically decide outgoing network and IP addresses by itself but when explicitly asked to use a specific one that curl cannot use, this error can occur.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Too many redirects. When following HTTP redirects, libcurl hit the maximum number set by the application. The maximum number of redirects is unlimited by libcurl but is set to 50 by default by the curl tool. The limit is present to stop endless redirect loops. Change the limit with <code>--max-redirs</code>.</p></li>
<li><p>Unknown option specified to libcurl. This could happen if you use a curl version that is out of sync with the underlying libcurl version. Perhaps your newer curl tries to use an option in the older libcurl that was not introduced until after the libcurl version you are using but is known to your curl tool code as that is newer. To decrease the risk of this and make sure it does not happen: use curl and libcurl of the same version number.</p></li>
<li><p>Malformed telnet option. The telnet options you provide to curl was not using the correct syntax.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>The server’s SSL/TLS certificate or SSH fingerprint failed verification. curl can then not be sure of the server being who it claims to be. See the <a href="usingcurl-tls.md">using TLS with curl</a> section for more TLS details and <a href="usingcurl-scpsftp.md">using SCP and SFTP with curl</a> for more SSH specific details.</p></li>
<li><p>The server did not reply anything, which in this context is considered an error. When a HTTP(S) server responds to a HTTP(S) request, it will always return <em>something</em> as long as it is alive and sound. All valid HTTP responses have a status line and responses header. Not getting anything at all back is an indication the server is faulty or perhaps that something prevented curl from reaching the right server or that you are trying to connect to the wrong port number etc.</p></li>
<li><p>SSL crypto engine not found.</p></li>
<li><p>Cannot set SSL crypto engine as default.</p></li>
<li><p>Failed sending network data. Sending data over the network is a crucial part of most curl operations and when curl gets an error from the lowest networking layers that the sending failed, this exit status gets returned. To pinpoint why this happens, some serious digging is usually required. Start with enabling verbose mode, do tracing and if possible check the network traffic with a tool like Wireshark or similar.</p></li>
<li><p>Failure in receiving network data. Receiving data over the network is a crucial part of most curl operations and when curl gets an error from the lowest networking layers that the receiving of data failed, this exit status gets returned. To pinpoint why this happens, some serious digging is usually required. Start with enabling verbose mode, do tracing and if possible check the network traffic with a tool like Wireshark or similar.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Problem with the local certificate. The client certificate had a problem so it could not be used. Permissions? The wrong pass phrase?</p></li>
<li><p>Couldn’t use the specified SSL cipher.</p></li>
<li><p>Peer certificate cannot be authenticated with known CA certificates.</p></li>
<li><p>Unrecognized transfer encoding.</p></li>
<li><p>Invalid LDAP URL.</p></li>
<li><p>Maximum file size exceeded.</p></li>
<li><p>Requested FTP SSL level failed.</p></li>
<li><p>Sending the data requires a rewind that failed.</p></li>
<li><p>Failed to initialize SSL Engine.</p></li>
<li><p>The user name, password, or similar was not accepted and curl failed to log in.</p></li>
<li><p>File not found on TFTP server.</p></li>
<li><p>Permission problem on TFTP server.</p></li>
<li><p>Out of disk space on TFTP server.</p></li>
<li><p>Illegal TFTP operation.</p></li>
<li><p>Unknown TFTP transfer ID.</p></li>
<li><p>File already exists (TFTP).</p></li>
<li><p>No such user (TFTP).</p></li>
<li><p>Character conversion failed.</p></li>
<li><p>Character conversion functions required.</p></li>
<li><p>Problem with reading the SSL CA cert</p></li>
<li><p>The resource referenced in the URL does not exist.</p></li>
<li><p>An unspecified error occurred during the SSH session.</p></li>
<li><p>Failed to shut down the SSL connection.</p></li>
<li><p><strong>Not used</strong></p></li>
<li><p>Could not load CRL file, missing or wrong format</p></li>
<li><p>TLS certificate issuer check failed</p></li>
<li><p>The FTP PRET command failed</p></li>
<li><p>RTSP: mismatch of CSeq numbers</p></li>
<li><p>RTSP: mismatch of Session Identifiers</p></li>
<li><p>unable to parse FTP file list</p></li>
<li><p>FTP chunk callback reported error</p></li>
<li><p>No connection available, the session will be queued</p></li>
<li><p>SSL public key does not matched pinned public key</p></li>
<li><p>Invalid SSL certificate status</p></li>
<li><p>Stream error in HTTP/2 framing layer</p></li>
</ol>
<h2 id="error-message">Error message</h2>
<p>When curl exits with a non-zero code, it will also output an error message (unless <code>--silent</code> is used). That error message may add some additional information or circumstances to the exit status number itself so the same error number can get different error messages.</p>
<h2 id="not-used">“Not used”</h2>
<p>The list of exit codes above contains a number of values marked as ‘not used’. Those are exit status codes that are not used in modern versions of curl but that have been used or were intended to be used in the past. They may be used in a future version of curl.</p>
<p>Additionally, the highest used error status in this list is 92, but there is no guarantee that a future curl version will not add more exit codes after that number.</p>
