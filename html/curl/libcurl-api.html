<h2 id="api-compatibility">API compatibility</h2>
<p>libcurl promises API stability and guarantees that your program written today will remain working in the future. We do not break compatibility.</p>
<p>Over time, we add features, new options and new functions to the APIs but we do not change behavior in a non-compatible way or remove functions.</p>
<p>The last time we changed the API in an non-compatible way was for 7.16.0 in 2006 and we plan to never do it again.</p>
<h3 id="version-numbers">Version numbers</h3>
<p>Curl and libcurl are individually versioned, but they mostly follow each other rather closely.</p>
<p>The version numbering is always built up using the same system:</p>
<pre><code>X.Y.Z</code></pre>
<ul class="incremental">
<li>X is main version number</li>
<li>Y is release number</li>
<li>Z is patch number</li>
</ul>
<h3 id="bumping-numbers">Bumping numbers</h3>
<p>One of these X.Y.Z numbers will get bumped in every new release. The numbers to the right of a bumped number will be reset to zero.</p>
<p>The main version number X is bumped when <em>really</em> big, world colliding changes are made. The release number Y is bumped when changes are performed or things/features are added. The patch number Z is bumped when the changes are mere bugfixes.</p>
<p>It means that after a release 1.2.3, we can release 2.0.0 if something really big has been made, 1.3.0 if not that big changes were made or 1.2.4 if mostly bugs were fixed.</p>
<p>Bumping, as in increasing the number with 1, is unconditionally only affecting one of the numbers (and the ones to the right of it are set to zero). 1 becomes 2, 3 becomes 4, 9 becomes 10, 88 becomes 89 and 99 becomes 100. So, after 1.2.9 comes 1.2.10. After 3.99.3, 3.100.0 might come.</p>
<p>All original curl source release archives are named according to the libcurl version (not according to the curl client version that, as said before, might differ).</p>
<h3 id="which-libcurl-version">Which libcurl version</h3>
<p>As a service to any application that might want to support new libcurl features while still being able to build with older versions, all releases have the libcurl version stored in the <code>curl/curlver.h</code> file using a static numbering scheme that can be used for comparison. The version number is defined as:</p>
<pre><code>#define LIBCURL_VERSION_NUM 0xXXYYZZ</code></pre>
<p>Where XX, YY and ZZ are the main version, release and patch numbers in hexadecimal. All three number fields are always represented using two digits (eight bits each). 1.2.0 would appear as “0x010200” while version 9.11.7 appears as “0x090b07”.</p>
<p>This 6-digit hexadecimal number is always a greater number in a more recent release. It makes comparisons with greater than and less than work.</p>
<p>This number is also available as three separate defines: <code>LIBCURL_VERSION_MAJOR</code>, <code>LIBCURL_VERSION_MINOR</code> and <code>LIBCURL_VERSION_PATCH</code>.</p>
<p>These defines are, of course, only suitable to figure out the version number built <em>just now</em> and they will not help you figuring out which libcurl version that is used at run-time three years from now.</p>
<h3 id="which-libcurl-version-runs">Which libcurl version runs</h3>
<p>To figure out which libcurl version that your application is using <em>right now</em>, <code>curl_version_info()</code> is there for you.</p>
<p>Applications should use this function to judge if things are possible to do or not, instead of using compile-time checks, as dynamic/DLL libraries can be changed independent of applications.</p>
<p>curl_version_info() returns a pointer to a struct with information about version numbers and various features and in the running version of libcurl. You call it by giving it a special age counter so that libcurl knows the “age” of the libcurl that calls it. The age is a define called <code>CURLVERSION_NOW</code> and is a counter that is increased at irregular intervals throughout the curl development. The age number tells libcurl what struct set it can return.</p>
<p>You call the function like this:</p>
<p>curl_version_info_data *ver = curl_version_info( CURLVERSION_NOW );</p>
<p>The data will then be pointing at struct that has or at least can have the following layout:</p>
<pre><code>struct {
  CURLversion age;          /* see description below */

  /* when &#39;age&#39; is 0 or higher, the members below also exist: */
  const char *version;      /* human readable string */
  unsigned int version_num; /* numeric representation */
  const char *host;         /* human readable string */
  int features;             /* bitmask, see below */
  char *ssl_version;        /* human readable string */
  long ssl_version_num;     /* not used, always zero */
  const char *libz_version; /* human readable string */
  const char * const *protocols; /* protocols */

  /* when &#39;age&#39; is 1 or higher, the members below also exist: */
  const char *ares;         /* human readable string */
  int ares_num;             /* number */

  /* when &#39;age&#39; is 2 or higher, the member below also exists: */
  const char *libidn;       /* human readable string */

  /* when &#39;age&#39; is 3 or higher (7.16.1 or later), the members below also
     exist  */
  int iconv_ver_num;       /* &#39;_libiconv_version&#39; if iconv support enabled */

  const char *libssh_version; /* human readable string */

} curl_version_info_data;</code></pre>
